import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import {withRouter} from 'react-router-dom'
import axios from 'axios';
import Button from 'react-bootstrap/Button';

const validEmailRegex = RegExp(
  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
);
const validateForm = errors => {
  let valid = true;
  Object.values(errors).forEach(val => val.length > 0 && (valid = false));
  return valid;
};

class Register extends Component {
  constructor(props){
    super(props);
    this.state={
      firstname:'',
      lastname:'',
      email:'',
      password:'',
      errors:{  
        firstname:'First name Cannot be empty.',
        lastname:'Last name Cannot be empty.',
        email:'Email Cannot be empty.',
        password:'Password Cannot be empty.',
        mobileno:'mobile no Cannot be empty.', 
      }
    }
  }

  handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    let errors = this.state.errors;

    switch (name) {
      case 'firstname': 
        errors.firstname = 
          value.length < 2
            ? 'First name be at least 2 characters long!'
            : '';
        break;
      case 'lastname': 
        errors.lastname = 
          value.length < 2
            ? 'Last Name must be at least 2 characters long!'
            : '';
        break;
      case 'email': 
        errors.email = 
          validEmailRegex.test(value)
            ? ''
            : 'Email is not valid!';
        break;
      case 'password': 
        errors.password = 
          value.length < 8
            ? 'Password must be at least 8 characters long!'
            : '';
        break;
      case 'mobileno': 
        errors.mobileno = 
          value.length != 10
            ? 'mobile no. must be 10 characters long!'
            : '';
        break;      
      default:
        break;
    }

    this.setState({errors, [name]: value});
  }

  handleClick(event){
      event.preventDefault();
      if(validateForm(this.state.errors)) {
      var apiBaseUrl = "http://localhost:4000/";
      console.log("values",this.state.first_name,this.state.last_name,this.state.email,this.state.password);
      //To be done:check for empty values before hitting submit
      const payload={
      "firstname": this.state.firstname,
      "lastname":this.state.lastname,
      "email":this.state.email,
      "password":this.state.password,
      "mobileno":this.state.mobileno

      }
      console.log(payload);
      axios.post(apiBaseUrl+'customersignin', payload)
    .then(function (response) {
      console.log(response.data.code);
      if(response.data.code === 200){
          console.log("registration successfull");
          window.location.assign('/login');
      }
      else if(response.data.code === 500){
        window.location.assign('/signin');
      }
      else if(response.data.code === 409)
      {
        window.alert("already registered");
      }
    })
    .catch(function (error) {
      console.log(error);
    });
    }
    else{
      let message = '';
      Object.values(this.state.errors).forEach(val => val.length > 0 && message.length<1 && (message=val));
      window.alert(message);
    }
  }

  render() {
    const mystyle = {
      color: "white",
      backgroundColor: "DodgerBlue",
      padding: "10px",
      fontFamily: "Arial"
    };
    const mystyle1 = {
      color: "blue",
      padding: "10px",
      fontFamily: "Arial"
    };
    return (
      <div>
        <MuiThemeProvider>
          <div>
          <b><h3 style={mystyle1}>Register</h3></b>
           <TextField
             hintText="Enter your First Name"
             floatingLabelText="First Name"
             name="firstname"

             onChange = {this.handleChange}
             />
           <br/>
           <TextField
             hintText="Enter your Last Name"
             floatingLabelText="Last Name"
             name="lastname"

             onChange = {this.handleChange}
             />
           <br/>
           <TextField
             hintText="Enter your Email"
             type="email"
             floatingLabelText="Email"
             name="email"

             onChange = {this.handleChange}
             />
           <br/>
           <TextField
             type = "password"
             hintText="Enter your Password"
             floatingLabelText="Password"
             name="password"
             onChange = {this.handleChange}
             />
           <br/>
           <TextField
            type="number"
             hintText="Enter your Mobile No"
             floatingLabelText="Mobile No"
             name="mobileno"
             onChange = {this.handleChange}
             />
             <br />
           <Button style={mystyle} label="Submit" primary={true} style={style} onClick={(event) => this.handleClick(event)}>Submit</Button>

          </div>
         </MuiThemeProvider>
      </div>
    );
  }
}
const style = {
  margin: 15,
};
export default withRouter(Register);