import React, { Component } from 'react';
import axios from 'axios';
import {withRouter} from 'react-router-dom'

class Printorder extends Component {
    
    render() {
        console.log(this.props);
        const itemslist=this.props.value.items;
        let total=0;
        const status=this.props.value.status;
        const len=itemslist.length;
        for(var i=0;i<len;i++)
        {
            total+=itemslist[i].qty*itemslist[i].price;
        }
        return(
           
            <div>
                Restaurant Name:- {this.props.value.restaurant}
        <table align="center" cellPadding="10px"><tr><th>Name</th><th>Quantity</th><th>Final price</th></tr> {itemslist.map(item=><tr><td>{item.name}</td><td>{item.qty}</td><td>{item.qty*item.price}</td></tr>)}
             
        </table><div>Total = {total}</div>
        {status==0 ? <p>Status : Waiting for restaurant to accept the order</p> : null}
        {status==1 ? <p>Status : Food is being prepared</p> : null}
        {status==2 ? <p>Status : Delivered</p> : null}
        {status==3 ? <p>Status : Rejected</p> : null}
            </div>
        )
    }
}

export default Printorder;