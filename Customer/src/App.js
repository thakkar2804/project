import React from 'react';
import { BrowserRouter as Router, Route, Switch,Link } from "react-router-dom";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Login from './Login'
import Leftpanel from "./drawer";
import Register from './Register'
import servererror from './Servererror'
import Dashboard from './Dashboard'
import Welcome from './Welcome'
import Print from './PrintMenu'
import Logout from './Logout'
import Yourcart from './Yourcart'
import Yourorders from './yourorders'
import profile from './profile';
import Editprofile from './Editprofile';
import aboutus from './aboutus';

function App() {
  return (
    <Router>
      <div className="App">
      <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link className="nav-link" to={"/login"}>Login</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to={"/signin"}>Sign up</Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>

          <Leftpanel />
         {/* <Welcome />  */}
         <Switch>
          <Route path="/" exact component={Welcome} />
          <Route path="/login" component={Login} />
          <Route path="/servererror" component={servererror} />
          <Route path="/signin" component={Register} />
          <Route path='/dashboard' component={Dashboard} />
          <Route path="/printmenu" component={Print} />  
          <Route path="/yourcart" component={Yourcart} />  
          <Route path="/logout" component={Logout}/>
          <Route path="/yourorders" component={Yourorders} />
          <Route path='/profile' component={profile} />
          <Route path='/editprofile' component={Editprofile} />
          <Route path='/aboutus' component={aboutus} />
         </Switch>
      </div>
      </Router>
    
  );
}

export default App;
