import React, { Component } from 'react';
import axios from 'axios';
import {withRouter} from 'react-router-dom'
import { subscribeUser } from './subscription';


class Yourcart extends Component {
    constructor(props){
      super(props);
      this.state={
        message:'',      
        items:[],
        restaurant:'',
        longitude:'',
        latitude:''
      }
    }
    
    componentDidMount(){
        var self = this;
        var apiBaseUrl = "http://localhost:4000/";
        var curToken = window.localStorage.getItem('token');
        console.log(curToken);
        const payload={
            token : curToken,
        }
        axios.post(apiBaseUrl + 'getcart', payload).
        then(function (response) {
          console.log(response.data);
            if(response.data.code === 200)
            {
                if(response.data.cart!=null)
                {
                    var temp = response.data.cart.items;
                    var len = response.data.cart.items.length;
                    for(var i=0 ; i < len; i++){
                        temp[i].qty = 1;
                    }
                    self.setState({ code : 200,   
                        items : temp,
                        restaurant : response.data.cart.restaurant,
                        longitude:response.data.longitude,
                        latitude:response.data.latitude
                    });
                }
                //console.log('list = '+response.data.list);
                return true;
            }
            else
            {
                
                window.alert("Please Login First");
                window.location.assign('/login');
                return false;
            }
        })
    }

    handleIncrease = (event) => {
        var array = [...this.state.items];
        for(var i=0;i<array.length;i++)
        {
            if(array[i].id == event.currentTarget.id)
            {
                array[i].qty = array[i].qty + 1;    
            }
        }
        this.setState({
            items : array
        })
    }

    handlDecrease = (event) => {
        var array = [...this.state.items];
        for(var i=0;i<array.length;i++)
        {
            if(array[i].id == event.currentTarget.id)
            {
                if(array[i].qty > 1)
                {
                    array[i].qty = array[i].qty - 1;
                }    
            }
        }
        this.setState({
            items : array
        })
    }

    handleDelete = (event) => {
        var array = [...this.state.items];
        var finalarray = [];
        for(var i=0;i<array.length;i++)
        {
            if(array[i].id == event.currentTarget.id)
            {
                    
            }
            else
            {
                finalarray.push(array[i]);
            }
        }
        this.setState({
            items : finalarray
        })
    }

    handleSubmit(event){
        var apiBaseUrl = "http://localhost:4000/";
        var self=this;
        this.setState({
          unsaved:false
        });
        var curToken = window.localStorage.getItem('token');
        const payload={
          "restaurant" : this.state.restaurant,
          "items" : this.state.items,
          "vendor" : this.props.history.location.state.backUrl,
          "longitude" : this.state.longitude,
          "latitude" : this.state.latitude,
          "token" : curToken 
        }
        console.log(this.props.history.location.state.backUrl);
        
        axios.post(apiBaseUrl+'addorder', payload)
       .then(function (response) {
         console.log(response);
         if(response.data.code === 200){
           window.location.assign('/yourorders');
         }
         else if(response.data.code === 204)
            {
                
                window.alert("Please Login First");
                window.location.assign('/login');
                return false;
            }
       })
       .catch(function (error) {
         console.log(error);
        //  window.location.assign('/servererror');
       });
      }

    render() {
        let message=this.state.code;
        const restaurant=this.state.restaurant;
        let items=this.state.items;
        let total = 0;
        for(var i=0;i<items.length;i++)
        {
            total+=items[i].qty*items[i].price;
        }
        if(items.length==0)
        items=null;
          return (
            <div>
                {message ? <div>{items?<div><b>{restaurant}</b><br />
                        <table align="center" cellPadding="15px"><tr><th>Name</th><th>Price</th><th>Quantity</th><th></th></tr>{items.map(cart => (<tr><td>{cart.name}</td> <td>{cart.price}</td><td><button id={cart.id} onClick={this.handlDecrease}>-</button> {cart.qty} <button id={cart.id} onClick={this.handleIncrease}>+</button></td><td><button id={cart.id} onClick={this.handleDelete}>Delete</button></td></tr>))}</table><br />
          <b>Final Bill</b>
          <table align="center" cellPadding="15px"><tr><th>Item Name</th><th>Quantity</th><th>Final Price</th><th></th></tr>{items.map(cart => (<tr><td>{cart.name}</td> <td>{cart.qty}</td><td>{cart.price*cart.qty} </td></tr>))}</table><br />
          Total = {total}<br /><br />
          <button onClick={(event => this.handleSubmit(event))}>Order Now</button>
          </div>:<h1>No items in your cart</h1>}</div>
                    :<h1>No items in your cart</h1>}
            </div>
          );
    }
}

export default withRouter(Yourcart);