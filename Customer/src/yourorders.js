import React, { Component } from 'react';
import axios from 'axios';
import Printorder from './Printorder';

class yourorders extends Component {
    constructor(props){
        super(props);
        this.state={
          message:'',
          neworders : [],      
          liveorder : [],
          prevorder : []
        }
      }

    componentDidMount(){
        var self = this;
        var apiBaseUrl = "http://localhost:4000/";
        var curToken = window.localStorage.getItem('token');
        const payload={
            token : curToken,
        }
        axios.post(apiBaseUrl + 'getorder', payload).
        then(function(response){
            if(response.data.code===200)
            {
                console.log(response.data);
                self.setState({
                    message : response.data.code,
                    neworders : response.data.neworders,
                    liveorder :response.data.livorder,
                    prevorder : response.data.prevorder 
                })
                
            }
            else
            {
                window.alert("Please Login First");
                window.location.assign('/login');
                return false;
            }
        })
            
        
    }

    render() {
        let neworders = this.state.neworders;
            let liveorder=this.state.liveorder;
            let prevorder=this.state.prevorder;
            const msg=this.state.message;
            if(liveorder.length==0)
            liveorder=null;
            if(neworders.length==0)
            neworders=null;
            if(prevorder.length==0)
            prevorder=null;
            return (
                <div>
                     {msg ? <div><h1>New orders</h1> <br/>{neworders ? <div>{neworders.map(order=>(<Printorder value={order}/>))}</div>:<h1>No New Orders</h1>}<br/> 
                     <h1>Live orders</h1> <br/>{liveorder ? <div>{liveorder.map(order=>(<Printorder value={order}/>))}</div>:<h1>No Live Orders</h1>}<br/>
            <h1>Previous Orders </h1>{prevorder ? <div>{prevorder.map(order => (<div><Printorder value={order}/></div>))}</div>:<h1>No previous orders</h1>}</div>: <h1>Loading</h1> }
                </div>
            )
    }
}

export default yourorders;