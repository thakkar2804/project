import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: '36ch',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
}));

export default function AlignItemsList(props) {
  const classes = useStyles();
  const restauranttype=props.list.restaurantType;
  console.log(restauranttype)
  return (
    <List className={classes.root}>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar alt={props.list.restaurant} src={props.list.image} style={{ height: '60px', width: '60px', margin:'7px' }} variant="square" />
        </ListItemAvatar>
        <ListItemText
          style={{marginTop: '10px'}}
          primary={props.list.restaurant}
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                {restauranttype ? <div>{restauranttype}</div> : <div>North Indian</div>}
              </Typography>
              
          {props.list.distance ? <div>{props.list.distance} km</div>:<div>0.1 km</div>}
            </React.Fragment>
          }
        />
      </ListItem>
    </List>
  );
}