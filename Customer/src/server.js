var mongoose=require("mongoose");
var express=require("express");
var jwt = require('jwt-simple');
var cors = require('cors')
var app=express();
var bodyParser = require('body-parser')
var bcrypt=require("bcrypt");
var distance=require("spherical-geometry-js");
var googleapis=require("googleapis");
mongoose.connect("mongodb://localhost/food_order_app");

const secretkey="jhbsdkjsbkjdsbfkjdsbf";

app.use(cors()) 

app.use(bodyParser.urlencoded({
  extended: true
}))

app.use(bodyParser.json());

var itemSchema2 = mongoose.Schema({
	name:String, 
	price:Number
});

var signInVendorSchema=new mongoose.Schema({
	firstname:String,
	lastname:String,
	restaurant:String,
	restaurantType:String, 
	email:String,
	password:String,
	mobileno:String,
	longitude:Number,
	latitude:Number,
	image:String,
	items:[itemSchema2],
	orders: [
		{
		  type: mongoose.Schema.Types.ObjectId,
		  ref: "Order",
		},
	  ]
})
var itemSchema = mongoose.Schema({
	id:String,
	name:String, 
	price:Number
},{ _id : false });

var cartSchema = mongoose.Schema({
	isOrdered: {type:Boolean},
	vendor: String,
	restaurant: String,
	items: [itemSchema]
})

var Cart=mongoose.model("Cart",cartSchema);

var Vendor=mongoose.model("vendor",signInVendorSchema);

var signInCustomerSchema=new mongoose.Schema({
	firstname:String,
	lastname:String,
	email:String,
	password:String,
	mobileno:Number,
	longitude:Number,
	latitude:Number,
	orders: [
		{
		  type: mongoose.Schema.Types.ObjectId,
		  ref: "Order",
		},
	  ],
	  cart: cartSchema,
});

var Customer=mongoose.model("customer",signInCustomerSchema);

var itemSchema1 = mongoose.Schema({
	id:String,
	name:String, 
	price:Number,
	qty:Number
},{ _id : false });

var OrderSchema=new mongoose.Schema({
	items:[itemSchema1],
	status: { type: Number, default: 0 },
	restaurant:String,
	longitude:Number,
	latitude:Number,
	customer:{
		id: {
			type: mongoose.Schema.Types.ObjectId,
			ref:"customer",
		}
	},
	vendor:{
		id: {
			type:mongoose.Schema.Types.ObjectId,
			ref:"vendor"
		}
	}
});

var Order=mongoose.model("order",OrderSchema);

function checktoken(req)
{
	if(req.body.token == null)
	{
		var response = {
			message : "false",
			code : 204
		}
		return response;
	}
	else
	{
		//console.log(req.body.token);
		var decodedToken = jwt.decode(req.body.token,secretkey);
		//console.log(decodedToken);
		if(decodedToken.isLoggedIn != true)
		{
			var response = {
				//token: decodedToken,
				message : "false",
				code : 204
			}
			return response;
		}
		else
		{
			if(decodedToken.isCustomer == true)
			{
				var response = {
					message : "true",
					token : decodedToken ,
					code : 200
				}
				return response;
			}
			else
			{
				var response = {
					message : "false",
					code : 204
				}
				return response
			}
		}
	}
	
}

app.post("/customersignin",function(req,res){
	
	console.log(req.body);
	// console.log(req)
	var savenow=new Customer(req.body);
	Customer.find({
		email:savenow.email
	},function(err,data){
		if(err)
		console.log(err);
		else
		{
			
			if(data.length>0)
			{
				var response={
					code:409
				}
				res.send(response);
				console.log(response);
			}
			else
			{
				savenow.password=bcrypt.hashSync(savenow.password, 12);
				savenow.save(function(err,Data){
					if(err)
					{  
						var response={
							code:204
						}
						res.send(response);
					}
					else {
						var response={
							code:200
						}
						res.send(response);
					}
				});
			}
		}
	});
	
});

app.post("/customerlogin",function(req,res){
	
    var savenow=new Customer(req.body);
    console.log(req.body);
	Customer.find({
		email:savenow.email
	},function(err,data){
		if(err)
		console.log(err);
		else
		{
            if(data.length>0)
            {
                if(bcrypt.compareSync(savenow.password,data[0].password))
                {
					var restoken={
						userId:data[0]._id,
						isLoggedIn:true,
						isCustomer:true
					}
	
					var encodedToken=jwt.encode(restoken, secretkey);

					var response={
						token:encodedToken,
						code:200
					}
                    res.send(response);
                    console.log(response);
                }
                else
                {
					var response={
						code:401
					}
                    res.send(response);
                    console.log(response);
                }
                
            }
            else
            {
				var response={
					code:401
				}
				res.send(response);
				console.log(response);
            }
		}
	});
	
});

function deg2rad(deg) {
	return deg * (Math.PI/180)
  };

app.post("/dashboard",function(req,res){
	var auth=checktoken(req);
	console.log(req.body);
	if(auth.code==204)
	{
		auth.message="you are not logged in";
		res.send(auth);
	}
	else
	{
		var decodedToken = jwt.decode(req.body.token,secretkey);
		console.log(decodedToken.userId);
		Customer.findOne({'_id':decodedToken.userId},function(err,data){
			data.longitude = req.body.longitute;
			data.latitude = req.body.latitude;
			data.save();
			console.log(data);
		})
			Vendor.find(function(err,data)
			{
				if(err)
				console.log(err);
				else
				{
					var response = {
						code:200,
						message:"Welcome Customer",
						list:[]
					}
	//						console.log(data[0].longitude);
					for(var i in data)
					{
						var tempOb=data[i].toObject();

						
						var	lat2 = tempOb.latitude;
						var	lon2 = tempOb.longitude;
						
						var	lat1 = req.body.latitude;
						var	lon1 = req.body.longitute;

						var R = 6371;
						var dLat = deg2rad(lat2-lat1);  // deg2rad below
						var dLon = deg2rad(lon2-lon1); 
						var a = 
							Math.sin(dLat/2) * Math.sin(dLat/2) +
							Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
							Math.sin(dLon/2) * Math.sin(dLon/2)
							; 
						var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
						var d = R * c; // Distance in km
						var imageloc = "/images/"+tempOb.image;

						var temp = {
							id:data[i]._id,
							image:imageloc,
							restaurant : tempOb.restaurant,
							longitude : tempOb.longitude,
							latitude : tempOb.latitude,
							restaurantType:tempOb.restaurantType,
							status:tempOb.status,
							distance:d.toFixed(1)
						}
						response.list.push(temp);
					}
					res.send(response);
				}
			})
		}	
});

app.post("/printmenu",function(req,res)
{
	var auth=checktoken(req);
	console.log(auth);

	if(auth.code==204)
	{
		auth.message="you are not logged in";
		res.send(auth);
	}
	else
	{
		Vendor.find({
			_id:req.body.vendorid
		},function(err,data)
		{
			if(err)
			console.log(err);
			else{
				var tempOb=data[0].toObject();
				console.log(tempOb);
				var temp={
					items : tempOb.items,
					image : "/images/"+tempOb.image 
				}
				console.log(tempOb.mobileno)
				var response={
					code:200,
					restaurant : tempOb.restaurant,
					mobileno : tempOb.mobileno,
					list: temp
				}
				res.send(response);
			}
		})
	}
})

app.post("/addtocart",function(req,res){
	var auth=checktoken(req);
	console.log(auth);
	console.log(req.body);
	var savenow=new Cart(req.body);
	if(auth.code==204)
	{
		auth.message="you are not logged in";
		res.send(auth);
	}
	else
	{

		var decodedToken = jwt.decode(req.body.token,secretkey);
		Customer.findOneAndUpdate(
			{_id:decodedToken.userId},
			{cart:savenow},
		function(err,data){
			if(err)
			console.log(err);
		});
		var response={
			code:200
		}
		res.send(response);
	}
});

app.post("/getcart",function(req,res)
{
	var auth=checktoken(req);
	if(auth.code==204)
	{
		auth.message="you are not logged in";
		res.send(auth);
	}
	else
	{
		Customer.find({
			_id:auth.token.userId
		},function(err,data)
		{
			if(err)
			console.log(err);
			else{
				var tempOb=data[0].toObject();
				console.log(tempOb);
				var response={
					code:200,
					longitude:tempOb.longitude,
					latitude:tempOb.latitude,
					cart:tempOb.cart
				}
				res.send(response);
			}
		})
	}
})

app.post("/addorder",function(req,res){
	var auth=checktoken(req);
	var savenow=new Order(req.body);
	if(auth.code == 204)
	{
		auth.message="you are not logged in";
		res.send(auth);
	}
	else
	{

		var decodedToken = jwt.decode(req.body.token,secretkey);
		savenow.customer.id = decodedToken.userId;
		savenow.items = req.body.items;
		savenow.vendor.id = req.body.vendor;
		savenow.longitude = req.body.longitude;
		savenow.latitude = req.body.latitude;
		console.log(savenow);
		savenow.save();
		Customer.findOne(savenow.customer.id, function (err, data) {
			if (err) console.log(err);
			// Add the category to the user's `categories` array.
			data.orders.push(savenow);
			data.cart=null;
			data.save();
			console.log(data);
		  });
		Vendor.findOne(savenow.vendor.id, function (err, data) {
			if (err) console.log(err);
			// Add the category to the user's `categories` array.
			data.orders.push(savenow);
			data.save();
			console.log(data);
		  });
		  Customer.findOneAndUpdate({
			  _id : decodedToken.userId
		  },{cart : []},function(err,data){
			  if(err)
			  console.log(err);
			  else
			  {
				  console.log(data);
			  }
		  })
		  var response={
			code:200
		}
		res.send(response);
	}
});

app.post("/getorder",function(req,res){
	var auth=checktoken(req);
	console.log(decodedToken);
	if(auth.code == 204)
	{
		auth.message="you are not logged in";
		res.send(auth);
	}
	else
	{
	var decodedToken = jwt.decode(req.body.token,secretkey);
		var response={
			neworders : [],
			livorder : [],
			prevorder : [],
			code : 0,
		}
		response.code=auth.code;
		Order.find({
			"customer.id":decodedToken.userId},function(err,data)
		{
			if(err)
			console.log(err);
			else
			{
				for(var i=0;i<data.length;i++)
				{
					if(data[i].status==0)
					{
						response.neworders.push(data[i]);
					}
					else if(data[i].status==1)
					{
						response.livorder.push(data[i]);
						//console.log(data[i].isServed);
					}
					else
					{
						response.prevorder.push(data[i]);
					}
				}
				//console.log(data);
				res.send(response);
			}
		});	
	}
});

app.post('/editProfile',(req,res) => {
	var auth=checktoken(req);
	console.log(req.body);
	if(auth.code == 204)
	{
		var response={
			code:204
		}
		res.send(response);
	}
	var decodedToken = jwt.decode(req.body.token,secretkey);
	console.log(decodedToken.userId);
	Customer.findOne({'_id':decodedToken.userId},function(err,data){
		data.firstname = req.body.firstname;
		data.lastname = req.body.lastname;
		data.mobileno = req.body.mobileno;
		data.save();
		console.log(data);
	})
	var response = {
		code:200
	}
	res.send(response);
})

app.post('/profile',(req,res) => {
	var auth=checktoken(req);
	
	if(auth.code == 204)
	{
		var response={
			code:204
		}
		res.send(response);
	}
	else
	{
		var decodedToken = jwt.decode(req.body.token,secretkey);
		Customer.findOne({'_id':decodedToken.userId},function(err,data){
			if(err) {
				console.log(err);
			}
			else {
				var response={
					code:200,
					firstname:data.firstname,
					lastname:data.lastname,
					emailid:data.email,
					mobileno:data.mobileno
				}
				res.send(response);
			}
		})
	}
})

app.listen(4000,function(err){
	if(err)
		console.log("err");
})
