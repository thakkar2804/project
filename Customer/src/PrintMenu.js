import React, { Component } from 'react';
import {withRouter,Link, BrowserRouter as Router} from 'react-router-dom'
import axios from 'axios';
import ListMenu from './menulist';
import Button from 'react-bootstrap/Button';
import Check from '@material-ui/icons/Check'
import { Prompt } from 'react-router'

class PrintMenu extends Component {
    constructor(props){
      super(props);
      console.log(this.props.history.location.state.backUrl);
      this.state = {
        code : 0 ,
        restaurant : '',
        unsaved:false,
        items : [],
        finalCart: []
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    };

    componentDidMount(){
      var self=this;
        var apiBaseUrl = "http://localhost:4000/";
        var curToken = window.localStorage.getItem('token');
        const payload={
            token : curToken,
            vendorid: self.props.history.location.state.backUrl
        }
        axios.post(apiBaseUrl + 'printmenu', payload).
        then(function (response) {
          // console.log(response.data.list.items);
            if(response.data.code === 200)
            {
                var len = response.data.list.items.length;
                var temp = response.data.list.items;
                for(var i=0;i<len;i++)
                {
                  temp[i].flag = false;
                }

                // console.log(response.data.restaurant);
                self.setState({ code : 200,   
                message : response.data.message,
                restaurant : response.data.restaurant,
                mobileno : response.data.mobileno,
                items : temp
                });
                //console.log('list = '+response.data.list);
                return true;
            }
            else
            {   
                window.alert("Please Login First");
                window.location.assign('/login');
                return false;
            }
        })
    }

    handleSubmit(event){
      var apiBaseUrl = "http://localhost:4000/";
      var self=this;
      this.setState({
        unsaved:false
      });
      var curToken = window.localStorage.getItem('token');
      const payload={
        "restaurant" : this.state.restaurant,
        "items" : this.state.finalCart,
        "vendor" : this.props.history.location.state.backUrl,
        "token" : curToken 
      }
      console.log(payload);
      var backUrl = this.props.history.location.state.backUrl;
      axios.post(apiBaseUrl+'addtocart', payload)
     .then(function (response) {
       console.log(response);
       if(response.data.code === 200){
        self.props.history.push({
            pathname: '/yourcart',
            state:
            {
                backUrl
            }
        })
       }
       else
       {
         window.location.assign('/servererror');
       }
     })
     .catch(function (error) {
       console.log(error);
       window.location.assign('/servererror');
     });
    }
    

    handleadd = (event) => {
      this.setState({
        unsaved:false
      });
      console.log(event.currentTarget.id);
      var array = [...this.state.items];
      var finalarray = [];
      for(var i=0;i<array.length;i++)
      {
        if(array[i]._id == event.currentTarget.id)
        {
          if(array[i].flag == false)
          {
            array[i].flag = true;
          }
          else
          array[i].flag = false;
        }
        if(array[i].flag == true)
        {
          var obj = {
            id : array[i]._id,
            name : array[i].name,
            price : array[i].price
          } 
          finalarray.push(obj);
        }
      }
      if(finalarray.length>0)
      {
        this.setState({
          unsaved:true
        });
      }
      console.log(finalarray);
      this.setState({
        items : array,
        finalCart : finalarray
      })
      // array[index].flag = !array[index].flag;
      // this.setState({
      //   items : array
      // })
    }

    render() {
      const message=this.state.code;
      const items=this.state.items;
        return (
          <div>
            <Prompt
  when={this.state.unsaved}
  message="You have not added items in cart. Do you want to navigate?"
/>
        Mobile no:&nbsp;{this.state.mobileno}
                    {message ? <div><b>Menu</b><br />
                        <table align="center">{items.map(menu => (<tr id={menu._id}><td><ListMenu list={menu} /></td>
                        <td> <div>{menu.flag ? <Button variant="success" id={menu._id} onClick={this.handleadd}><Check /> Added</Button> : <Button id={menu._id} onClick={this.handleadd}> + Add</Button> 
                          }</div></td></tr>))}</table><br />
                          <div style={{flex:1}}>
        <div style={{borderWidth:1,position:'absolute',bottom:'450px',left:'920px',alignSelf:'flex-end',position:'fixed'}}>
           <Button
             title="Press"
             color="#841584"
             accessibilityLabel="Press"
             onClick={this.handleSubmit}>Add all selected items to cart</Button>
             
        </div>
      </div>
                   </div>
                    :<h1>Loading</h1>}
                </div>
        );
    }
}

export default withRouter(PrintMenu);