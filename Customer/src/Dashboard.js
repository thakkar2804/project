import React, { Component } from 'react';
import {withRouter,Link, BrowserRouter as Router} from 'react-router-dom'
import axios from 'axios';
import ListRestaurent from './restaurentlist';
import Button from 'react-bootstrap/Button';


class Dashboard extends Component {
    constructor(props){
        super(props);
        this.state = {
            message : '',
            code : false,
            list : []
        };
        this.makeAcall=this.makeAcall.bind(this);
        this.handleClick=this.handleClick.bind(this);
    }

    makeAcall(pos){
        var self=this;
        console.log(pos.longitude);
        var apiBaseUrl = "http://localhost:4000/";
        var curToken = window.localStorage.getItem('token');
        console.log(curToken);
        const payload={
            token : curToken,
            longitute: pos.longitude,
            latitude : pos.latitude
        }
        axios.post(apiBaseUrl + 'dashboard', payload).
        then(function (response) {
            if(response.data.code === 200)
            {
                self.setState({ code : 200,   
                message : response.data.message,
                list : response.data.list
                });
                //console.log('list = '+response.data.list);
                return true;
            }
            else
            {
                
                window.alert("Please Login First");
                window.location.assign('/login');
                return false;
            }
        })
    };

    handleClick = (event) => {
        event.preventDefault();
        console.log(event.currentTarget.id);
        var backUrl = event.currentTarget.id;
        this.props.history.push({
            pathname: '/printmenu',
            state:
            {
                backUrl
            }
        })
    };

    handlemap = (event) => {
        var lng = event.currentTarget.id;
        var lat = event.currentTarget.getAttribute('id1');
        window.open("https://maps.google.com?q="+lat+","+lng );
    }

    async componentDidMount(){
        var self=this;
        navigator.geolocation.getCurrentPosition(function(position){
            console.log(position)
            let pos = {
                longitude: position.coords.longitude,
                latitude: position.coords.latitude
            }
            self.makeAcall(pos);
        },function(error){

        }, {maximumAge:60000, timeout:5000, enableHighAccuracy:true});
            
          
    };

    render() {
        const message = this.state.message;
        const listofrest = this.state.list;
        const mystyle = {
            cursor:"pointer"
          };
        console.log(listofrest);
            return (
                <div>
                     {message ? <div><b>Nearby Restuarants</b><br/>
                        <table align="center"><tr></tr>{listofrest.map(name => (<tr id={name.id}><td onClick={this.handleClick} style={mystyle} id={name.id}>{name.status == false?null:<ListRestaurent list={name}/>}</td>
                        <td>{name.status == false?null: <Button id={name.longitude} id1={name.latitude} onClick={this.handlemap}>View in map</Button>} </td>
                        </tr>))}</table><br />

                    </div>
                    : <h1>Loading</h1>}
                </div>
            )
    }
}

export default withRouter(Dashboard);