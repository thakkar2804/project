import React from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Login from './Login'
import Register from './Register'
import servererror from './Servererror'
import Dashboard from './Dashboard'
import Welcome from './Welcome'
import AddItem from './AddItem'
import Leftpanel from "./drawer";
import Logout from './Logout';
import Yourorders from './yourorders';
import Edititem from './Edititem';
import profile from './profile';
import status from './status';
import aboutus from './aboutus';
import customer from './customer';
import Editprofile from './Editprofile';

function App() {
  return (
    <Router>
      <div className="App">
      <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link className="nav-link" to={"/login"}>Login</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to={"/signin"}>Sign up</Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>

          <Leftpanel />
         {/* <Welcome />  */}
        <Switch>
          <Route path="/" exact component={Welcome} />
          <Route path="/login" component={Login} />
          <Route path="/servererror" component={servererror} />
          <Route path="/signin" component={Register} />
          <Route path='/dashboard' component={Dashboard} />  
          <Route path='/additem' component={AddItem} />
          <Route path='/logout' component={Logout} />
          <Route path='/yourorders' component={Yourorders}/>
          <Route path='/edititem' component={Edititem} />
          <Route path='/profile' component={profile} />
          <Route path='/status' component={status} />
          <Route path='/aboutus' component={aboutus} />
          <Route path='/customer' component={customer} />
          <Route path='/editprofile' component={Editprofile} />
        </Switch>  
      </div>
      </Router>
    
  );
}

export default App;
