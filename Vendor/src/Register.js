import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import axios from 'axios';
import { geolocated } from "react-geolocated";
import FormData from 'form-data'
import {withRouter} from 'react-router-dom'
import Button from 'react-bootstrap/Button';

const validEmailRegex = RegExp(
  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
);
const validateForm = errors => {
  let valid = true;
  Object.values(errors).forEach(val => val.length > 0 && (valid = false));
  return valid;
};

class Register extends Component {
  constructor(props){
    super(props);
    this.state={
      firstname:'',
      lastname:'',
      email:'',
      password:'',
      restaurant:'',
      myimage:'',
      restaurantType:'',
      mobileno:'',
      errors:{  
        firstname:'First name Cannot be empty.',
        lastname:'Last name Cannot be empty.',
        email:'Email Cannot be empty.',
        password:'Password Cannot be empty.',
        restaurant:'Restaurent name Cannot be empty.',
        restaurantType:'Restaurent type Cannot be empty.',
        mobileno:'mobile no Cannot be empty.',  
        myimage:'Image field Cannot be empty.',
      }
    }
    this.handleImageChange = this.handleImageChange.bind(this);

  }
  handleImageChange(event) {
    let error = this.state.errors.myimage='';
    error = '';
    this.setState(
      {error}
    )
    this.setState({image: event.target.files[0]});
  }

  handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    let errors = this.state.errors;

    switch (name) {
      case 'firstname': 
        errors.firstname = 
          value.length < 2
            ? 'First name be at least 2 characters long!'
            : '';
        break;
      case 'lastname': 
        errors.lastname = 
          value.length < 2
            ? 'Last Name must be at least 2 characters long!'
            : '';
        break;
      case 'email': 
        errors.email = 
          validEmailRegex.test(value)
            ? ''
            : 'Email is not valid!';
        break;
      case 'password': 
        errors.password = 
          value.length < 8
            ? 'Password must be at least 8 characters long!'
            : '';
        break;
      case 'restaurant': 
        errors.restaurant = 
          value.length < 3
            ? 'restaurant name must be at least 3 characters long!'
            : '';
        break;
      case 'restaurantType': 
        errors.restaurantType = 
          value.length < 1
            ? 'restaurant type must be at least 1 characters long!'
            : '';
      case 'mobileno': 
        errors.mobileno = 
          value.length != 10
            ? 'mobile no. must be 10 characters long!'
            : '';
        break;      
      default:
        break;
    }

    this.setState({errors, [name]: value});
  }

  handleClick(event){
    event.preventDefault();
    if(validateForm(this.state.errors)) {
      var apiBaseUrl = "http://localhost:4006/";
      var self = this;
      console.log("values",this.state.first_name,this.state.last_name,this.state.email,this.state.password);
      //To be done:check for empty values before hitting submit
      console.log(this.props.coords.longitude);
      let formData = new FormData();
      formData.append('firstname',this.state.firstname);
      formData.append('lastname',this.state.lastname);
      formData.append('email',this.state.email);
      formData.append('password',this.state.password);
      formData.append('restaurant',this.state.restaurant);
      formData.append('restaurantType',this.state.restaurantType);
      formData.append('mobileno',this.state.mobileno);
      formData.append('longitude',this.props.coords.longitude);
      formData.append('latitude',this.props.coords.latitude);
      formData.append('myimage',this.state.image);
      axios.post(apiBaseUrl+'vendorsignin', formData)
    .then(function (response) {
      console.log(response.data.code);
      if(response.data.code === 200){
          console.log("registration successfull");
          window.location.assign('/login');
      }
      else if(response.data.code === 204){
        self.setState({
          printmessage: 'Internal error.'
        });
      }
      else if(response.data.code === 500){
        window.location.assign('/signin');
      }
      else if(response.data.code === 409)
      {
        self.setState({
          printmessage: 'Already Registered'
        });
        //window.alert("already registered");
      }
    })
    .catch(function (error) {
      self.setState({
        printmessage: 'Internal error. Only jpeg/png/jpg/gif is allowed'
      });
      console.log(error);
    });
    }else{
      let message = '';
      Object.values(this.state.errors).forEach(val => val.length > 0 && message.length<1 && (message=val));
      window.alert(message);
    }
  }

  render() {
    const mystyle = {
      color: "white",
      backgroundColor: "DodgerBlue",
      padding: "10px",
      fontFamily: "Arial"
    };
    const mystyle1 = {
      color: "blue",
      fontFamily: "Arial"
    };
    return (
      <div>
        <MuiThemeProvider>
          <div>
          <b><h3 style={mystyle1}>Register</h3></b>
           
           <TextField
             hintText="Enter your First Name"
             floatingLabelText="First Name"
             name="firstname"
             onChange = {this.handleChange}
             />
           <br/>
           <TextField
             hintText="Enter your Last Name"
             floatingLabelText="Last Name"
             name="lastname"
             onChange = {this.handleChange}
             />
           <br/>
           <TextField
             hintText="Enter your Email"
             type="email"
             floatingLabelText="Email"
             name="email"
             onChange = {this.handleChange}
             />
           <br/>
           <TextField
             type = "password"
             hintText="Enter your Password"
             floatingLabelText="Password"
             name="password"
             onChange = {this.handleChange}
             />
            <br />
            <TextField
             hintText="Enter your Restaurant name"
             floatingLabelText="Restaurant Name"
             name="restaurant"
             onChange = {this.handleChange}
             />
           <br/>
           <TextField
             hintText="Enter your Restaurant Type"
             floatingLabelText="Restaurent Type"
             name="restaurantType"
             onChange = {this.handleChange}
             />
           <br/>
           <TextField
             hintText="Enter your Mobile no"
             floatingLabelText="Mobile no."
             name="mobileno"
             onChange = {this.handleChange}
             />
           <br/>
           <br />
           <input type="file" name="myimage"
            onChange={this.handleImageChange}
           />
           <br/>
           <Button style={mystyle} label="Submit" primary={true} style={style} onClick={(event) => this.handleClick(event)}>Submit</Button>
          </div>
         </MuiThemeProvider>
      </div>
    );
  }
}
const style = {
  margin: 15,
};

export default withRouter(geolocated({
  positionOptions: {
    enableHighAccuracy: true
  },
  userDecisionTimeout: 5000
})(Register));