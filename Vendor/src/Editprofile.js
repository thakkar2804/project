import React, { Component } from 'react';
import {withRouter} from 'react-router-dom'
import axios from 'axios';

class Editprofile extends Component {
    constructor(props){
        super(props);
        console.log(this.props.history.location.state.backUrl);
        this.state = {
            firstname:this.props.history.location.state.backUrl.firstname,
            lastname:this.props.history.location.state.backUrl.lastname,
            restaurantname:this.props.history.location.state.backUrl.restaurantname,
            restauranttype:this.props.history.location.state.backUrl.restauranttype,
            mobileno:this.props.history.location.state.backUrl.mobileno,
        }
        this.handleChange=this.handleChange.bind(this);
        this.handleChange1=this.handleChange1.bind(this);
        this.handleChange2=this.handleChange2.bind(this);
        this.handleChange3=this.handleChange3.bind(this);
        this.handleChange4=this.handleChange4.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
    };

    handleChange(event) {
        this.setState({firstname: event.target.value});
      }

    handleChange1(event) {
        this.setState({lastname: event.target.value});
      }
    
    handleChange2(event) {
        this.setState({restaurantname: event.target.value});
      }

    handleChange3(event) {
        this.setState({restauranttype: event.target.value});
      }

    handleChange4(event) {
        this.setState({mobileno: event.target.value});
      }

    handleSubmit(event) {
        var apiBaseUrl = "http://localhost:4006/";
        var curToken = window.localStorage.getItem('token');
        const payload={
              token : curToken,
              firstname: this.state.firstname,
              lastname:this.state.lastname,
              restaurantname:this.state.restaurantname,
              restauranttype:this.state.restauranttype,
              mobileno:this.state.mobileno,
            }
            console.log(payload);
            axios.post(apiBaseUrl+'editProfile', payload)
           .then(function (response) {
             console.log(response);
             if(response.data.code === 200){
                window.location.assign('/profile');
             }
             else if(response.data.code === 204)
            {
                
                window.alert("Please Login First");
                window.location.assign('/login');
                return false;
            }
           })
           .catch(function (error) {
             console.log(error);
        });
    }
          
        

    render() {
        const item=this.props.history.location.state.backUrl;
        return (
        <div>
            <table align="center" cellPadding="10px">
                <tr><td><label>
                First Name:</label></td><td>
                <input type="text" value={this.state.firstname} onChange={this.handleChange} />
                </td></tr><tr>
                <td><label>
                Last Name:</label></td><td>
                <input type="text" value={this.state.lastname} onChange={this.handleChange1} />
                </td>
                </tr>
                <tr><td>
                <label>
                Restaurant Name:</label></td><td>
                <input type="text" value={this.state.restaurantname} onChange={this.handleChange2} />
                </td>
                </tr>
                <tr><td>
                <label>
                Restaurant Type:</label></td><td>
                <input type="text" value={this.state.restauranttype} onChange={this.handleChange3} />
                </td>
                </tr>
                <tr><td>
                <label>
                Mobile No:</label></td><td>
                <input type="text" value={this.state.mobileno} onChange={this.handleChange4} />
                </td>
                </tr>
            </table>
            <button onClick={this.handleSubmit}>Submit</button>
        </div>
        );
    
    }

};
export default withRouter(Editprofile);