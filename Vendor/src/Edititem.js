import React, { Component } from 'react';
import {withRouter} from 'react-router-dom'
import axios from 'axios';
import Button from 'react-bootstrap/Button'


class Edititem extends Component {
    constructor(props){
        super(props);
        console.log(this.props.history.location.state.backUrl);
        this.state = {
            itemname:this.props.history.location.state.backUrl.name,
            itemprice:this.props.history.location.state.backUrl.price
        }
        this.handleChange=this.handleChange.bind(this);
        this.handleChange1=this.handleChange1.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
        this.handleDelete=this.handleDelete.bind(this);

    };

    handleChange(event) {
        this.setState({itemname: event.target.value});
      }

    handleChange1(event) {
        this.setState({itemprice: event.target.value});
      }
    
    handleSubmit(event) {
        var apiBaseUrl = "http://localhost:4006/";
        var curToken = window.localStorage.getItem('token');
        const payload={
              token : curToken,
              id: this.props.history.location.state.backUrl.id,
              name:this.state.itemname,
              price:this.state.itemprice
            }
            console.log(payload);
            axios.post(apiBaseUrl+'editItem', payload)
           .then(function (response) {
             console.log(response);
             if(response.data.code === 200){
                window.location.assign('/dashboard');
             }
             else if(response.data.code === 204)
              {
                  
                  window.alert("Please Login First");
                  window.location.assign('/login');
                  return false;
              }
           })
           .catch(function (error) {
             console.log(error);
        });
    }
    
    handleDelete(event) {
        var apiBaseUrl = "http://localhost:4006/";
        var curToken = window.localStorage.getItem('token');
        const payload={
              token : curToken,
              id: this.props.history.location.state.backUrl.id,
              name:this.state.itemname,
              price:this.state.itemprice
            }
            console.log(payload);
            axios.post(apiBaseUrl+'deleteItem', payload)
           .then(function (response) {
             console.log(response);
             if(response.data.code === 200){
                window.location.assign('/dashboard');
             }
             else if(response.data.code === 204)
              {
                  
                  window.alert("Please Login First");
                  window.location.assign('/login');
                  return false;
              }
           })
           .catch(function (error) {
             console.log(error);
        });
    }
        

    render() {
        const item=this.props.history.location.state.backUrl;
        return (
        <div>
            <label>
            Item Name:
            <input type="text" value={this.state.itemname} onChange={this.handleChange} />
            </label><br />
            <label>
            Item Price:
            <input type="text" value={this.state.itemprice} onChange={this.handleChange1} />
            </label>
            <br />
            <Button variant="outline-success" onClick={this.handleSubmit}>Submit</Button>&nbsp;
            <Button variant="outline-danger" onClick={this.handleDelete}>Delete Item</Button>
        </div>
        );
    
    }

};
export default withRouter(Edititem);