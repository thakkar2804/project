var mongoose=require("mongoose");
var express=require("express");
var jwt = require('jwt-simple');
var cors = require('cors');
var dotenv = require('dotenv')
var webpush = require('web-push')
var bodyParser = require('body-parser');
var bcrypt=require("bcrypt");
var fs = require('fs'); 
var path = require('path');
var multer = require('multer');

const secretkey="jhbsdkjsbkjfkjdsbf";
const public_key="BGElKguYKVJhdZmxtMNl7xBz7FF_7i6rG3Dnhm26VYn3n_NfHgzHAS1LoEkEgmYraKovKy7wkIiPTCSmJjie2cg";
const private_key="WzIgHBrdHEsVpjd_BJ5bq5DHYa1DATwxFGlhbh-B2io";
const we_push_contact="mailto: contact@my-site.com";

mongoose.connect("mongodb://localhost/food_order_app");

var app=express();

dotenv.config();

app.use(cors()) 
app.use(bodyParser.urlencoded({
  extended: true
}))

app.use(bodyParser.json());

webpush.setVapidDetails(we_push_contact,public_key,private_key);

app.use('/uploads', express.static('uploads'));

var storage = multer.diskStorage({ 
    destination: (req, file, cb) => { 
        cb(null, 'uploads') ;
    }, 
    filename: (req, file, cb) => { 
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname)); 
    } 
}); 
  
var upload = multer({ 
	storage: storage,
	limits:{fileSize:10000000},
	fileFilter: function(req,file,cb){
		checkFileType(file,cb);
	} 
}).single('myimage'); 

function checkFileType(file,cb){
	const filetypes = /jpeg|jpg|png|gif/;
	const extname = filetypes.test(path.extname(file.originalname).toLowerCase());

	const mimetype = filetypes.test(file.mimetype);

	if(mimetype && extname){
		return cb(null,true);
	} else {
		cb("Error:Images Only!");
	}

}
// var imageSchema = mongoose.Schema({
// 	name: String, 
//     desc: String, 
//     img: 
//     { 
//         data: Buffer, 
//         contentType: String 
//     }
// });  
var itemSchema = mongoose.Schema({
	id:String,
	name:String, 
	price:Number
},{ _id : false });

var cartSchema = mongoose.Schema({
	isOrdered: {type:Boolean},
	vendor: String,
	restaurant: String,
	items: [itemSchema]
})

var Cart=mongoose.model("Cart",cartSchema);

var itemSchema = mongoose.Schema({
	name:String, 
	price:Number
});

var signInCustomerSchema=new mongoose.Schema({
	firstname:String,
	lastname:String,
	email:String,
	password:String,
	mobileno:Number,
	longitude:Number,
	latitude:Number,
	orders: [
		{
		  type: mongoose.Schema.Types.ObjectId,
		  ref: "Order",
		},
	  ],
	  cart: cartSchema,
});

var Customer=mongoose.model("customer",signInCustomerSchema);

var signInVendorSchema=new mongoose.Schema({
	firstname:String,
	lastname:String,
	restaurant:String,
	restaurantType:String, 
	email:String,
	password:String,
	mobileno:String,
	longitude:Number,
	latitude:Number,
	status:{ type: Boolean, default: true },
	image:String,
	items:[itemSchema]
})

var Vendor=mongoose.model("vendor",signInVendorSchema);

var itemSchema1 = mongoose.Schema({
	id:String,
	name:String, 
	price:Number,
	qty:Number
},{ _id : false });

var OrderSchema=new mongoose.Schema({
	items:[itemSchema1],
	status: { type: Number, default: 0 },
	restaurant:String,
	customer:{
		id: {
			type: mongoose.Schema.Types.ObjectId,
			ref:"customer",
		}
	},
	vendor:{
		id: {
			type:mongoose.Schema.Types.ObjectId,
			ref:"vendor"
		}
	}
});

var Order=mongoose.model("order",OrderSchema);

function checktoken(req)
{
	if(req.body.token == null)
	{
		var response = {
			message : "false",
			code : 204
		}
		return response;
	}
	else
	{
		//console.log(req.body.token);
		var decodedToken = jwt.decode(req.body.token,secretkey);
		//console.log(decodedToken);
		if(decodedToken.isLoggedIn != true)
		{
			var response = {
				//token: decodedToken,
				message : "false",
				code : 204
			}
			return response;
		}
		else
		{
			if(decodedToken.isVendor == true)
			{
				var response = {
					message : "true",
					token : decodedToken ,
					code : 200
				}
				return response;
			}
			else
			{
				var response = {
					message : "false",
					code : 204
				}
				return response
			}
		}
	}
	
}

app.post("/vendorsignin",upload,function(req,res){
	var savenow=new Vendor(req.body);
	savenow.image=req.file.filename;
	console.log(req.body);
	console.log(savenow);
	console.log(req.file);
	Vendor.find({
		email:savenow.email
	},function(err,data){
		if(err)
		console.log(err);
		else
		{
			
			if(data.length>0)
			{
				var response={
					code:409
				}
				var deleteFile = "uploads/"+savenow.image;
				fs.unlinkSync(deleteFile);
				res.send(response);
				console.log(response);
			}
			else
			{
				savenow.password=bcrypt.hashSync(savenow.password, 12);	
				savenow.save(function(err,Data){
					if(err)
					{  
						var response={
							code:204
						}
						var deleteFile = "uploads/"+savenow.image;
						fs.unlinkSync(deleteFile);
						res.send(response);
					}
					else {
						var response={
							code:200
						}
						var deleteFile = "uploads/"+savenow.image;
						var cpyfile = "../../Customer/public/images/"+savenow.image;
						fs.copyFile(deleteFile,cpyfile,function(err){
							if(err)
							console.log(err);
						});
						res.send(response);
					}
				});
			}
		}
	});
	
});

app.post("/vendorlogin",function(req,res){
	
    var savenow=new Vendor(req.body);
    console.log(req.body);
	Vendor.find({
		email:savenow.email
	},function(err,data){
		if(err)
		console.log(err);
		else
		{
            if(data.length>0)
            {
                if(bcrypt.compareSync(savenow.password,data[0].password))
                {
					console.log(data[0]._id);
					var restoken={
						userId:data[0]._id,
						isLoggedIn:true,
						isVendor:true
					}
	
					var encodedToken=jwt.encode(restoken, secretkey);

					var response={
						token:encodedToken,
						code:200
					}
                    res.send(response);
                    //console.log(response);
                }
                else
                {
					var response={
						code:401
					}
                    res.send(response);
                    console.log(response);
                }
                
            }
            else
            {
				var response={
					code:401
				}
				res.send(response);
				console.log(response);
            }
		}
	});
	
});

app.post("/vendordashboard",function(req,res){
	var auth=checktoken(req);

	if(auth.code==204)
	{
		auth.message="you are not logged in";
		res.send(auth);
	}
	else
	{
		var response={
			code : 200,
			message:"",
			token : "",
			items : []
		}
		var decodedToken= jwt.decode(req.body.token,secretkey);
		response.token=decodedToken;
		console.log(response.token.userId);
		Vendor.find({
			_id:response.token.userId
		},function(err,data){
			if(err)
			console.log(err)
			else
			{ 
				//console.log(data);
				var temp=data[0].toObject();
				response.items=temp.items;
				res.send(response);
			}
		});
		
	}

})

app.post("/additem",function(req,res){
	console.log(req.body);
	var response=checktoken(req);
	if(response.code==204)
	{
		response.message="you are not loggedin";
		console.log(response);
		res.send(response);
	}
	else
	{
		// console.log(req.body.token);
		// var decodedToken = jwt.decode(req.body.token,secretkey);
		// console.log(decodedToken);
		// if(decodedToken.isLoggedIn != true)
		// {
		// 	var response = {
		// 		message : "you are not loggedin",
		// 		code : 204
		// 	}
		// 	res.send(response);
		// }
		// else
		// {
			var decodedToken = jwt.decode(req.body.token,secretkey);
			if(decodedToken.isVendor == true)
			{
				Vendor.findOneAndUpdate(
					{ _id: decodedToken.userId }, 
					// {items:[]},
					{ $addToSet: { items: { $each: req.body.items }  } },
				function (error, success) {
						if (error) {
							console.log(error);
						} else {
							console.log(success);
						}
					});
				// var response = {
				// 	message : "Welcome Vendor",
				// 	code : 200
				// }
				response.message="hello vendor";
				console.log(response.token);
				res.send(response);
			}
			else
			{
				response.message="not vendor";
				console.log(response.token);
				res.send(response);
			}
		// }
	}
});

app.post("/getorder",function(req,res){
	var auth=checktoken(req);
	console.log(decodedToken);
	if(auth.code == 204)
	{
		auth.message="you are not logged in";
		res.send(auth);
	}
	else
	{
		var decodedToken = jwt.decode(req.body.token,secretkey);
		var response={
			neworders : [],
			livorder : [],
			prevorder : [],
			code : 0,
		}
		response.code=auth.code;
		Order.find({
			"vendor.id":decodedToken.userId},function(err,data)
		{
			if(err)
			console.log(err);
			else
			{
				for(var i=0;i<data.length;i++)
				{
					if(data[i].status==0)
					{
						response.neworders.push(data[i]);
					}
					else if(data[i].status==1)
					{
						response.livorder.push(data[i]);
						//console.log(data[i].isServed);
					}
					else
					{
						response.prevorder.push(data[i]);
					}
				}
				console.log(response);
				res.send(response);
			}
		});	
	}
});

app.post('/editItem',(req,res) => {
	var auth=checktoken(req);
	console.log(req.body);
	if(auth.code == 204)
	{
		var response={
			code:204
		}
		res.send(response);
	}
	var decodedToken = jwt.decode(req.body.token,secretkey);
	console.log(decodedToken.userId);
	Vendor.findOne({'_id':decodedToken.userId},function(err,data){
		for(var i=0;i<data.items.length;i++)
		{
			if(data.items[i]._id == req.body.id)
			{
				console.log("inside");
				data.items[i].name = req.body.name;
				data.items[i].price = req.body.price;
				break;
			}
		}
		data.save();
		console.log(data);
	})
	var response = {
		code:200
	}
	res.send(response);
})

app.post('/deleteItem',(req,res) => {
	var auth=checktoken(req);
	console.log(req.body);
	if(auth.code == 204)
	{
		var response={
			code:204
		}
		res.send(response);
	}
	var decodedToken = jwt.decode(req.body.token,secretkey);
	console.log(decodedToken.userId);
	Vendor.findOne({'_id':decodedToken.userId},function(err,data){
		var list = [];
		for(var i=0;i<data.items.length;i++)
		{
			if(data.items[i]._id == req.body.id)
			{
				continue;
			}
			list.push(data.items[i]);
		}
		data.items = list;
		data.save();
		console.log(data);
	})
	var response = {
		code:200
	}
	res.send(response);
})

app.post('/editProfile',(req,res) => {
	var auth=checktoken(req);
	console.log(req.body);
	if(auth.code == 204)
	{
		var response={
			code:204
		}
		res.send(response);
	}
	var decodedToken = jwt.decode(req.body.token,secretkey);
	console.log(decodedToken.userId);
	Vendor.findOne({'_id':decodedToken.userId},function(err,data){
		data.firstname = req.body.firstname;
		data.lastname = req.body.lastname;
		data.restaurant = req.body.restaurantname;
		data.restaurantType = req.body.restauranttype;
		data.mobileno = req.body.mobileno;
		data.save();
		console.log(data);
	})
	var response = {
		code:200
	}
	res.send(response);
})

app.post('/profile',(req,res) => {
	var auth=checktoken(req);
	if(auth.code == 204)
	{
		var response={
			code:204
		}
		res.send(response);
	}
	var decodedToken = jwt.decode(req.body.token,secretkey);
	Vendor.findOne({'_id':decodedToken.userId},function(err,data){
		if(err) {
			console.log(err);
		}
		else {
			var response={
				code:200,
				firstname:data.firstname,
				lastname:data.lastname,
				restaurantname:data.restaurant,
				restauranttype:data.restaurantType,
				image:data.image,
				mobileno:data.mobileno
			}
			res.send(response);
		}
	})
})

app.post('/getStatus',(req,res) => {
	var auth=checktoken(req);
	var decodedToken = jwt.decode(req.body.token,secretkey);
	if(auth.code == 204)
	{
		var response={
			code:204
		}
		res.send(response);
	}
	Vendor.findOne({'_id':decodedToken.userId},function(err,data){
		if(err) {
			console.log(err);
		}
		else {
			var response={
				code:200,
				status:data.status
			}
			res.send(response);
		}
	})
})

app.post('/changeStatus',(req,res) => {
	var auth=checktoken(req);
	if(auth.code == 204)
	{
		var response={
			code:204
		}
		res.send(response);
	}
	var decodedToken = jwt.decode(req.body.token,secretkey);
	Vendor.findOne({'_id':decodedToken.userId},function(err,data){
		if(err) {
			console.log(err);
		}
		else {
			data.status = !data.status;
			data.save();
			var response={
				code:200,
				status:data.status
			}
			res.send(response);
		}
	})
})


app.post('/temp',(req,res) => {
	res.redirect('http://localhost:3006/subscribe');
})

app.post('/subscribe', (req, res) => {
	const subscription = req.body
  
	console.log(subscription)
  
	const payload = JSON.stringify({
	  title: 'New Order came!',
	  body: 'Please checkout from your orders',
	})
  
	webpush.sendNotification(subscription, payload)
	  .then(result => console.log(result))
	  .catch(e => console.log(e.stack))
  
	res.status(200).json({'success': true})
  });

  app.post("/changeorderstatus",function(req,res){
	var auth=checktoken(req);
	console.log(decodedToken);
	if(auth.code == 204)
	{
		auth.message="you are not logged in";
		res.send(auth);
	}
	else
	{
		var decodedToken = jwt.decode(req.body.token,secretkey);
		console.log("printing req.body"+req.body);
		var response={
			neworders : [],
			livorder : [],
			prevorder : [],
			code : 0,
		}
		response.code=auth.code;
		var currstatus=req.body.status;
		if(currstatus==0)
		{
			Order.updateOne({_id:req.body.id},{status:1},function(err,data)
			{
				if(err)
				{
					console.log(err);
				}
				
				//res.send(response);
			})
		}
		else if(currstatus==1)
		{
			Order.updateOne({_id:req.body.id},{status:2},function(err,data)
			{
				if(err)
				{
					console.log(err);
				}
				
				//res.send(response);
			})
		}
		else if(currstatus==3)
		{
			Order.updateOne({_id:req.body.id},{status:3},function(err,data)
			{
				if(err)
				{
					console.log(err);
				}
				
				//res.send(response);
			})
		}
		Order.find({
			"vendor.id":decodedToken.userId},function(err,data)
		{
			if(err)
			console.log(err);
			else
			{
				for(var i=0;i<data.length;i++)
				{
					if(data[i].status==0)
					{
						response.neworders.push(data[i]);
					}
					else if(data[i].status==1)
					{
						response.livorder.push(data[i]);
						//console.log(data[i].isServed);
					}
					else
					{
						response.prevorder.push(data[i]);
					}
				}
				console.log(response);
				res.send(response);
			}
		});	
	}
});

app.post('/getcustomer',(req,res) => {
	var auth=checktoken(req);
	
	if(auth.code == 204)
	{
		var response={
			code:204
		}
		res.send(response);
	}
	else
	{
		Customer.findOne({'_id':req.body.id},function(err,data){
			if(err) {
				console.log(err);
			}
			else {
				var response={
					code:200,
					firstname:data.firstname,
					lastname:data.lastname,
					emailid:data.email,
					mobileno:data.mobileno
				}
				res.send(response);
			}
		})
	}
})

app.listen(4006,function(err){
	if(err)
		console.log("err");
})
