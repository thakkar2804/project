import React, { Component } from 'react';
import axios from 'axios';
import Printorder from './Printorder';
import Button from 'react-bootstrap/Button'

class yourorders extends Component {
    constructor(props){
        super(props);
        this.state={
          message:'', 
          neworders : [],     
          liveorder : [],
          prevorder : []
        }
      }

    componentDidMount(){
        var self = this;
        var apiBaseUrl = "http://localhost:4006/";
        var curToken = window.localStorage.getItem('token');
        const payload={
            token : curToken,
        }
        axios.post(apiBaseUrl + 'getorder', payload).
        then(function(response){
            if(response.data.code===200)
            {
                console.log(response.data);
                self.setState({
                    message : response.data.code,
                    neworders : response.data.neworders,
                    liveorder :response.data.livorder,
                    prevorder : response.data.prevorder 
                })
                
            }
            else
            {
                window.alert("Please Login First");
                window.location.assign('/login');
                return false;
            }
        })
            
        
    }

    handleCustomer(event){
        event.preventDefault();
        var backUrl = {
            longitude:event.currentTarget.getAttribute('longitude'),
            latitude:event.currentTarget.getAttribute('latitude'),
            id:event.currentTarget.id
        }
        console.log(backUrl);
        this.props.history.push({
            pathname: '/customer',
            state:
            {
                backUrl
            }
        })

    }

    handleClick(event)
    {
        var self=this;
        var apiBaseUrl = "http://localhost:4006/";
        var curToken = window.localStorage.getItem('token');
        const payload={
            token : curToken,
            status : event.currentTarget.getAttribute("status"),
            id : event.currentTarget.id
        }
        axios.post(apiBaseUrl + 'changeorderstatus', payload)
        .then(function (response) {
            
            if(response.data.code === 200)
            {
                console.log(response.data);
                //window.localStorage.removeItem('token');
                self.setState({    
                    message : response.data.code,
                    neworders : response.data.neworders,
                    liveorder :response.data.livorder,
                    prevorder : response.data.prevorder
                });
                console.log(response.data);
                return true;
            }
            else if(response.data.code === 204)
            {
                
                window.alert("Please Login First");
                window.location.assign('/login');
                return false;
            }
        })
    }

    render() {
            let neworders = this.state.neworders; 
            let liveorder=this.state.liveorder;
            let prevorder=this.state.prevorder;
            const msg=this.state.message;
            if(liveorder.length==0)
            liveorder=null;
            if(neworders.length==0)
            neworders=null;
            if(prevorder.length==0)
            prevorder=null;
            return (
                <div>
                     {msg ? <div><h1>New orders</h1> <br/>{neworders ? <div>{neworders.map(order=>(<div><Printorder value={order}/> <Button variant="outline-success" status={order.status} id={order._id} onClick={event=>this.handleClick(event)}> Accept </Button>
                     &nbsp;<Button variant="outline-danger" status="3" id={order._id} onClick={event=>this.handleClick(event)}> Reject </Button>&nbsp;<Button variant="outline-primary" longitude={order.longitude} latitude={order.latitude} id={order.customer.id} onClick={event=>this.handleCustomer(event)}> Customer Details </Button><br /></div>))}</div>:<h1>No New Orders</h1>}
                     
                    <br/> <br />
                     <h1>Live orders</h1> <br/>{liveorder ? <div>{liveorder.map(order=>(<div><Printorder value={order}/><Button variant="outline-primary" status={order.status} id={order._id} onClick={event=>this.handleClick(event)}> Complete </Button>&nbsp;<Button variant="outline-warning" longitude={order.longitude} latitude={order.latitude} id={order.customer.id} onClick={event=>this.handleCustomer(event)}> Customer Details </Button><br /></div>))}</div>:<h1>No Live Orders</h1>}
                     <br/><br />
            <h1>Previous Orders </h1>{prevorder ? <div>{prevorder.map(order => (<div><Printorder value={order}/><Button variant="outline-dark" longitude={order.longitude} latitude={order.latitude} id={order.customer.id} onClick={event=>this.handleCustomer(event)}> Customer Details </Button><br /><br /><br /></div>))}</div>:<h1>No previous orders</h1>}</div>: <h1>Loading</h1> }
            
                </div>
            )
    }
}

export default yourorders;