import React, { Component } from 'react';
import {withRouter} from 'react-router-dom'
import axios from 'axios';
import Image from 'react-bootstrap/Image'


class profile extends Component {
    constructor(props){
        super(props);
        this.state = {
            firstname:'',
            lastname:'',
            restaurantname:'',
            restauranttype:'',
            image:'',
            mobileno:''
        }
        this.handleEdit = this.handleEdit.bind(this);
    };
    
    componentDidMount() {
        var self = this;
        var apiBaseUrl = "http://localhost:4006/";
        var curToken = window.localStorage.getItem('token');
        const payload={
              token : curToken
            }
            console.log(payload);
            axios.post(apiBaseUrl+'profile', payload)
           .then(function (response) {
             console.log(response);
             if(response.data.code === 200){
                self.setState({
                    firstname:response.data.firstname,
                    lastname:response.data.lastname,
                    restaurantname:response.data.restaurantname,
                    restauranttype:response.data.restauranttype,
                    image:require("./uploads/"+response.data.image),
                    mobileno:response.data.mobileno,

                })
                
             }
             else if(response.data.code === 204)
            {
                
                window.alert("Please Login First");
                window.location.assign('/login');
                return false;
            }
           })
           .catch(function (error) {
             console.log(error);
        });
    }

    handleEdit(event){
        event.preventDefault();
        var backUrl = {
            firstname:this.state.firstname,
            lastname:this.state.lastname,
            restaurantname:this.state.restaurantname,
            restauranttype:this.state.restauranttype,
            mobileno:this.state.mobileno
        }
        console.log(backUrl);
        this.props.history.push({
            pathname: '/editprofile',
            state:
            {
                backUrl
            }
        })

    }

    render() {
        const firstname = this.state.firstname;
        const lastname = this.state.lastname;
        const restaurantname = this.state.restaurantname;
        const restauranttype = this.state.restauranttype;
        const image = this.state.image;
        const mobileno = this.state.mobileno;
        return (
        <div>
            <img src={image} height="100px" width="100px" /><br /><br />
            <table align="center" cellPadding="10px">
                <tr>
                    <td><label>First Name</label></td>
                    <td>{firstname}</td>
                </tr>
                <tr>
                    <td><label>Last Name</label></td>
                    <td>{lastname}</td>
                </tr><tr>
                    <td><label>Restaurant Name</label></td>
                    <td>{restaurantname}</td>
                </tr><tr>
                    <td><label>Restaurant Type</label></td>
                    <td>{restauranttype}</td>
                </tr><tr>
                    <td><label>Mobile No.</label></td>
                    <td>{mobileno}</td>
                </tr>
            </table>
            <button onClick={this.handleEdit}>Edit Profile</button>
        </div>
        );
    
    }

};
export default withRouter(profile);