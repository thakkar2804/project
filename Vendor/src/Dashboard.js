import React, { Component } from 'react';
import axios from 'axios';
import {withRouter} from 'react-router-dom'

class Dashboard extends Component {
    constructor(props){
        super(props);
        this.state = {
            code : 0 ,
            items : []
        };
        this.handleEdit=this.handleEdit.bind(this);
    }

    componentDidMount(){
        var self=this;
        var apiBaseUrl = "http://localhost:4006/";
        var curToken = window.localStorage.getItem('token');
        console.log(curToken);
        const payload={
            token : curToken,
        }
        axios.post(apiBaseUrl + 'vendordashboard', payload)
        .then(function (response) {
            
            if(response.data.code === 200)
            {
                console.log(response.data);
                //window.localStorage.removeItem('token');
                self.setState({    
                    code : 200 ,
                    items : response.data.items
                });
                console.log(response.data);
                return true;
            }
            else if(response.data.code === 204)
            {
                
                window.alert("Please Login First");
                window.location.assign('/login');
                return false;
            }
        })
    }

    handleClick(event){
        window.location.assign("/additem");
    }

    handleEdit(event){
        event.preventDefault();
        var backUrl = {
            id:event.currentTarget.getAttribute('data'),
            name:event.currentTarget.getAttribute('data1'),
            price:event.currentTarget.getAttribute('data2'),
        }
        console.log(backUrl);
        this.props.history.push({
            pathname: '/edititem',
            state:
            {
                backUrl
            }
        })

    }

    render() {
        const message=this.state.code;
        const items=this.state.items;
        console.log(items[0]);
        
            return (
                <div>
                    {message ? <div><b>Menu</b><br />
                        <table align="center" cellPadding="10px"><tr><th>Name</th><th>Price</th></tr>{items.map(menu => (<tr><td>{menu.name}</td> <td>{menu.price}</td><td><button data={menu._id} data1={menu.name} data2={menu.price} onClick={this.handleEdit}>Edit Item</button></td></tr>))}</table><br />
                    <button onClick={(event => this.handleClick(event))}>Add item</button></div>
                    :<h1>Loading</h1>}
                </div>
            )
    }
}

export default withRouter(Dashboard);