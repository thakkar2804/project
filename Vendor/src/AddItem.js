import React from "react";
import axios from 'axios';
import Button from 'react-bootstrap/Button'

class AddItem extends React.Component{
    state = {
        items:[{name:"",price:""}]
    }

    handleChange = (e) => {
        if (["name", "price"].includes(e.target.className) ) {
          let items = [...this.state.items]
          items[e.target.dataset.id][e.target.className] = e.target.value
          this.setState({ items }, () => console.log(this.state.items))
        }
      }

    addItem = (e) => {
        console.log(this.state.items);
        this.setState((prevState) => ({
        items: [...prevState.items, {name:"", price:""}],
    }));
    }

    handleSubmit = (e) => { 
        e.preventDefault();
        let items = [...this.state.items];
        var payload = {
            "token":window.localStorage.getItem('token'),
            "items":items 
        }
        var apiBaseUrl = "http://localhost:4006/";
        axios.post(apiBaseUrl+'additem', payload)
        .then(function (response) {
        console.log(response);
        if(response.data.code === 200){
            window.alert("Data inserted");
            window.location.assign('/dashboard');
        }
        else
        {
            window.alert("Not stored");
        }
        })
        .catch(function (error) {
        console.log(error);
        });
    }


    render(){
        let {items} = this.state;
        return(
            <div onChange={this.handleChange}>
                {
                    items.map((val, idx)=> {
                        let itemId = `item-${idx}`, priceId = `price-${idx}`
                        return (
                        <div key={idx}>
                            <label htmlFor={itemId}>{`item #${idx + 1}`}</label>
                            <input
                            type="text"
                            name={itemId}
                            data-id={idx}
                            id={itemId}
                            value={items[idx].name} 
                            className="name"
                            />
                            <br />
                            <label htmlFor={priceId}>{`price #${idx + 1}`}</label>
                            <input
                            type="text"
                            name={priceId}
                            data-id={idx}
                            id={priceId}
                            value={items[idx].price} 
                            className="price"
                            />
                        </div>
                        )
                    })
                }
                <Button variant="outline-success" onClick={this.addItem}>Add Item</Button>&nbsp;
                <Button variant="outline-primary" onClick={this.handleSubmit}>Submit</Button>
            </div>
        )
    }
}

export default AddItem;