import React, { Component } from 'react';
import {withRouter} from 'react-router-dom'
import axios from 'axios';
import Button from 'react-bootstrap/Button'

class profile extends Component {
    constructor(props){
        super(props);
        this.state = {
            firstname:'',
            lastname:'',
            email:'',
            mobileno:'',
            longitude:this.props.history.location.state.backUrl.longitude,
            latitude:this.props.history.location.state.backUrl.latitude,
        }
        this.handleMap = this.handleMap.bind(this);
    };
    
    componentDidMount() {
        var self = this;
        var apiBaseUrl = "http://localhost:4006/";
        var curToken = window.localStorage.getItem('token');
        const payload={
              token : curToken,
              id : this.props.history.location.state.backUrl.id
            }
            console.log(payload);
            axios.post(apiBaseUrl+'getcustomer', payload)
           .then(function (response) {
             console.log(response);
             if(response.data.code === 200){
                self.setState({
                    firstname:response.data.firstname,
                    lastname:response.data.lastname,
                    email:response.data.emailid,
                    mobileno:response.data.mobileno,
                })
                
             }
             else if(response.data.code === 204)
            {
                
                window.alert("Please Login First");
                window.location.assign('/login');
                return false;
            }
           })
           .catch(function (error) {
             console.log(error);
        });
    }

    handleMap = (event) => {
        var lng = this.state.longitude;
        var lat = this.state.latitude;
        window.open("https://maps.google.com?q="+lat+","+lng );
    }

    render() {
        const firstname = this.state.firstname;
        const lastname = this.state.lastname;
        const email = this.state.email;
        const mobileno = this.state.mobileno;
        return (
        <div>
            <table align="center" cellPadding="15px">
                <tr>
                    <td><label>First Name</label></td>
                    <td>{firstname}</td>
                </tr>
                <tr>
                    <td><label>Last Name</label></td>
                    <td>{lastname}</td>
                </tr><tr>
                    <td><label>Email Id</label></td>
                    <td>{email}</td>
                </tr><tr>
                    <td><label>Mobile No.</label></td>
                    <td>{mobileno}</td>
                </tr>
            </table>
            <Button variant="outline-primary" onClick={this.handleMap}>View In Map</Button>
        </div>
        );
    
    }

};
export default withRouter(profile);