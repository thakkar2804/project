import React, { Component } from 'react';
import {withRouter} from 'react-router-dom'

class Welcome extends Component {

 handlelogin(event){
  this.props.history.push("/");
  window.location.assign('/login');
}

handleregister(event){
  this.props.history.push("/");
  window.location.assign('/signin');
}

render() {
  const Background = "./images/home-top-1.jpg";
  var sectionStyle = {
    marginTop : "-48px",
    width: "100%",
    height: "575px",
    backgroundImage: "url(" + Background + ")"
  };
    return (
      <section style={ sectionStyle }>
      <div>
        <button type="submit" onClick={(event) => this.handlelogin(event)}>Login</button><br />
        <button type="submit" onClick={(event) => this.handleregister(event)}>SignIn</button>
      </div>
      </section>
    );
  
}

};
export default withRouter(Welcome);