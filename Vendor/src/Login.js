import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import axios from 'axios';
import {withRouter} from 'react-router-dom'
import Button from 'react-bootstrap/Button';

class Login extends Component {
constructor(props){
  super(props);
  this.state={
  email:'',
  password:''
  }
 }

 handleClick(event){
  var apiBaseUrl = "http://localhost:4006/";
  var self=this;
  console.log("values",this.state.email,this.state.password);
  const payload={
  "email":this.state.email,
  "password":this.state.password
  }
  console.log(payload);
  axios.post(apiBaseUrl+'vendorlogin', payload)
 .then(function (response) {
   console.log(response);
   if(response.data.code === 200){
    window.localStorage.setItem('token',response.data.token);
    var token=window.localStorage.getItem('token');
    console.log(token);
    self.props.history.push("/dashboard");
    window.location.assign('/dashboard');
   }
   else
   {
     window.alert("Username or password didn't match");
   }
 })
 .catch(function (error) {
   console.log(error);
 });
}

render() {
  const mystyle = {
    color: "white",
    backgroundColor: "DodgerBlue",
    padding: "10px",
    fontFamily: "Arial"
  };
  const mystyle1 = {
    color: "blue",
    padding: "10px",
    fontFamily: "Arial"
  };
    return (
      <div>
        <MuiThemeProvider>
          <div>
          <b><h3 style={mystyle1}>Login</h3></b>

           <TextField
             hintText="Enter your Username"
             floatingLabelText="Username"
             onChange = {(event,newValue) => this.setState({email:newValue})}
             />
           <br/>
             <TextField
               type="password"
               hintText="Enter your Password"
               floatingLabelText="Password"
               onChange = {(event,newValue) => this.setState({password:newValue})}
               />
             <br/>
             <Button style={mystyle} label="Submit" primary={true} style={style} onClick={(event) => this.handleClick(event)}>Submit</Button>
             </div>
         </MuiThemeProvider>
      </div>
    );
  }
}
const style = {
 margin: 15,
};
export default withRouter(Login);