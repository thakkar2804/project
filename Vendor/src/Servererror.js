import React, { Component } from 'react';
class Servererror extends Component {
    render(){
        return (
            <div>
                <h1>500 Internal error! Please try again later</h1>
            </div>
        )
    }
}

export default Servererror;