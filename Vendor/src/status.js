import React, { Component } from 'react';
import {withRouter} from 'react-router-dom'
import axios from 'axios';

class status extends Component {
    constructor(props){
        super(props);
        this.state = {
            status:''
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    };
    
    componentDidMount() {
        var self = this;
        var apiBaseUrl = "http://localhost:4006/";
        var curToken = window.localStorage.getItem('token');
        const payload={
              token : curToken,
            }
            console.log(payload);
            axios.post(apiBaseUrl+'getStatus', payload)
           .then(function (response) {
             console.log(response);
             if(response.data.code === 200){
                if(response.data.status == true)
                {
                        self.setState({
                        status:'open'
                    })
                }
                else
                {
                    self.setState({
                        status:'close'
                    })
                }
             }
           })
           .catch(function (error) {
             console.log(error);
        });
    }
    
    handleSubmit(event) {
        event.preventDefault();
        var self = this;
        var apiBaseUrl = "http://localhost:4006/";
        var curToken = window.localStorage.getItem('token');
        const payload={
              token : curToken,
            }
            console.log(payload);
            axios.post(apiBaseUrl+'changeStatus', payload)
           .then(function (response) {
             console.log(response);
             if(response.data.code === 200){
                window.alert("Status changed successfully");
                if(self.state.status == "open")
                {
                    self.setState({
                        status:"close"
                    })
                }
                else
                {
                    self.setState({
                        status:"open"
                    })
                }
             }
           })
           .catch(function (error) {
             console.log(error);
        });
    }

    render() {
        return (
        <div>
            <label>
            Current Status:
            </label>
            &nbsp;{this.state.status}
            <br />
            <button onClick={this.handleSubmit}>Change status</button>
        </div>
        );
    
    }

};
export default withRouter(status);